﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Resources;
using System.Threading;
using System.Windows.Forms;

namespace svchost
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Form1 f = new Form1();
            //f.Show();
            //createNewWindow();
            //createNewWindow();
            //createNewWindow();
            //createNewWindow();
            Application.Run();
        }

        public static Bitmap randomizeImage() {
            Random r = new Random();
            int ran = r.Next(0, 16);
            List<Bitmap> imgs = new List<Bitmap>();
            imgs.Add(Properties.Resources.banana);
            imgs.Add(Properties.Resources.chicken);
            imgs.Add(Properties.Resources.frog);
            imgs.Add(Properties.Resources.gandalf);
            imgs.Add(Properties.Resources.lsd1);
            imgs.Add(Properties.Resources.lsd2);
            imgs.Add(Properties.Resources.lsd3);
            imgs.Add(Properties.Resources.snoop);
            imgs.Add(Properties.Resources.trolazo);
            imgs.Add(Properties.Resources.troll);
            imgs.Add(Properties.Resources.youareanidiot);
            imgs.Add(Properties.Resources.dance);
            imgs.Add(Properties.Resources.applause);
            imgs.Add(Properties.Resources.travolta);
            imgs.Add(Properties.Resources.pc);
            imgs.Add(Properties.Resources.ita);
            imgs.Add(Properties.Resources.cat);
            return imgs[ran];
        }

        static void createNewWindow() {
            Form1 f = new Form1();
            Random r1 = new Random();
            Random r2 = new Random();
            int width = Screen.PrimaryScreen.Bounds.Width - f.Width;
            int height = Screen.PrimaryScreen.Bounds.Height - f.Height;
            int posX = r1.Next(0, width);
            int posY = r2.Next(0, height);
            f.Show();
            f.SetDesktopLocation(posX, posY);
        }
    }
}
